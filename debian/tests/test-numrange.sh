#!/bin/bash

myret=0

# default test for numrandom
min=$(numrandom)
let max=$(numrandom)+100

myregex=/${min}..${max}/

numrange /${min}..${max}i3/  | numgrep $myregex || myret=1
numrange /${min}..${max}i5/  | numgrep $myregex || myret=1
numrange /${min}..${max}i13/ | numgrep $myregex || myret=1
numrange /${min}..${max}i19/ | numgrep $myregex || myret=1


# just another test
numrange /0..100i3/ | grep "33.*57.*90" || myret=1

if [ "$myret" -eq "1" ]; then
	false; 
fi
