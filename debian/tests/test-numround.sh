#!/bin/bash

myret=0

for i in $(seq 1 13); do 
	head -n${i} $AUTOPKGTEST_TMP/tests/rounds | tail -n1 | numround | grep "0" || let myret=$myret+1
done

if [ "$myret" -gt 0 ]; then
	false;
fi
